package com.cab404.dots.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.Scene;
import com.cab404.defense.ui.ListMenu;
import com.cab404.defense.ui.SelectableField;
import com.cab404.defense.ui.TableMenu;
import com.cab404.defense.ui.TextField;
import com.cab404.defense.util.FL;
import com.cab404.dots.events.EventListener;
import com.cab404.dots.events.MoveEvent;
import com.cab404.dots.events.OwnerChangeEvent;
import com.cab404.dots.game.logic.AI;
import com.cab404.dots.game.logic.Player;
import com.cab404.dots.game.processor.DotsProcessor;
import com.cab404.dots.game.processor.Marker;
import com.cab404.dots.render.DotsFieldRenderer;
import com.cab404.dots.render.Image;
import com.cab404.dots.render.Notification;
import com.cab404.dots.utils.A;
import com.cab404.dots.utils.Colors;
import com.cab404.dots.utils.SimpleObjectStorage;
import com.cab404.dots.utils.U;
import com.cab404.guidy.Luna;

import java.util.ArrayList;
import java.util.Collections;


/**
 * @author cab404
 */
public class GameScene implements Scene {

    private SimpleObjectStorage game, hud;
    private DotsFieldRenderer grid;
    private ListMenu status, notifications;
    private ArrayList<Player> players;
    private DotsProcessor core;
    private Image mini_map;
    private int move_delay = 0;

    private boolean game_over = false;


    public GameScene(int playerCount, int aiCount) {
        players = new ArrayList<>();

        for (int i = 0; i < playerCount; i++)
            players.add(new Player(Colors.colors[i]));

        for (int i = 0; i < aiCount; i++) {
            Player player = new Player(Colors.colors[i + 4]);
            player.isAI = true;
            players.add(player);
        }

        Collections.shuffle(players);


    }

    @Override
    public void create() {
        hud = new SimpleObjectStorage();

        status = new ListMenu() {
            @Override
            public void update(float time) {
                super.update(time);
                pos.set(parent.gfx.cam_size().sub(size));
            }
        };

        notifications = new ListMenu() {
            @Override
            public void update(float time) {
                super.update(time);
                pos.set(parent.gfx.convertToGFX(Luna.screen()).scl(0, 1).sub(size).scl(0, 1));
            }
        };

        hud.add(notifications, status);
        hud.gfx.cam.zoom = U.getHUDZoom(1.3f);
        hud.gfx.update();

        /*=*=*=*=*=*/

        core = new DotsProcessor(70, 70);
        core.addListener(new EventListener() {
            @Override
            public void onEvent(Object object) {
                if (object instanceof OwnerChangeEvent) {
                    OwnerChangeEvent e = (OwnerChangeEvent) object;

                    if (e.field.owner != null && e.field.owner != player().marker)
                        if (e.field.creator != null) {
                            player().can_place += 3;
                            cached_score += 3;
                        } else {
                            player().can_place += 1;
                            cached_score += 1;
                        }
                }
            }
        });

        game = new SimpleObjectStorage();
        game.gfx.cam.zoom = 1.2f;
        game.gfx.update();
//        game.gfx.cam.zoom = U.getHUDZoom(2.5f);

        /*=*=*=*=*=*/

        grid = new DotsFieldRenderer(core);
        grid.pipe.addListener(new EventListener() {
            @Override
            public void onEvent(Object object) {
                if (!player().isAI)
                    if (object instanceof MoveEvent) {
                        MoveEvent e = (MoveEvent) object;
                        rmx = e.x;
                        rmy = e.y;
                    }
            }
        });

        game.add(grid);

        /*=*=*=*=*=*/

        mini_map = new Image(new Sprite()) {
            @Override
            public void render(SpriteBatch batch) {
                super.render(batch);

                Vector2 position = grid.pos.cpy().scl(-(spr.getWidth() / grid.size.x));
                // Chain vector operations FTW.
                Vector2 size = grid.parent.gfx.cam_size().scl(1f / grid.size.x, 1f / grid.size.y).scl(spr.getWidth(), spr.getHeight());
                U.getSR().begin(batch);

                U.getSR().setColor(Colors.getBG(A.pointer_color));

                U.getSR().renderQuad(
                        position,
                        size);

                U.getSR().end();

            }
        };

        hud.add(mini_map);

        /*=*=*=*=*=*/

        resume();
        setStartMenu();

    }

    @Override
    public void update() {
        game.update(Luna.dt());
        hud.update(Luna.dt());

        if (move_delay <= 0) {
            Marker move = requestMove();

            if (move != null) {
                move(move.x, move.y);

                if (game_over) {
                    move_delay = Integer.MAX_VALUE;
                    gameOver();
                } else next();

            }
        } else {
            move_delay--;
        }
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.render();
        hud.render();
    }

    @Override
    public void dispose() {
    }

    @SuppressWarnings("unused")
    public void resume() {
        FL.reset();
        FL.getInstance().reloadFont(Gdx.files.internal("data/font/0403.ttf"), U.FONT_S, 16);
        FL.getInstance().reloadFont(Gdx.files.internal("data/font/0403.ttf"), U.FONT_M, 24);
        FL.getInstance().reloadFont(Gdx.files.internal("data/font/0403.ttf"), U.FONT_H, 32);
        updateMiniMap();
    }

    @Override
    public void resize(Vector2 size) {

    }

    protected Player player() {
        return players.get(current_player);
    }

    private int current_player;
    private int cached_score;


    //  Requested move x and requested move y.
    private int rmx = -1, rmy = -1;
    private boolean move_requested;
    private final Object calculations_lock = new Object();

    protected Marker requestMove() {
        if (player().isAI && !move_requested) {
            Marker calculated = AI.move(GameScene.this.player(), core);
            rmx = calculated.x;
            rmy = calculated.y;
            move_requested = true;
        }

        try {
            return core.get(rmx, rmy);
        } finally {
            if (rmx != -1 || rmy != -1)
                move_requested = false;
            rmx = rmy = -1;
        }
    }

    protected void move(int x, int y) {
        if (!game_over) {
            if (move_delay <= 0)
                if (core.move(player().marker, x, y)) {
                    player().can_place--;

                    // Showing added score
                    if (cached_score > 0)
                        notifications.add(new Notification("+" + cached_score, player().marker));
                    cached_score = 0;


                    // Switching player
                    boolean can_place = false;
                    for (int i = 0; i < core.w; i++)
                        for (int j = 0; j < core.h; j++)
                            if (core.get(i, j).owner == null) {
                                can_place = true;
                                break;
                            }
                    if (!can_place) {
                        gameOver();
                        return;
                    }

                    move_delay = A.frames_between_ai_moves;
                }
        }
    }

    private void next() {

        int full = 0;
        for (Player player : players)
            full += player.can_place;

        if (full == 0) {
            // Game over;
            gameOver();
            return;
        }

        current_player++;
        if (current_player >= players.size()) current_player = 0;

        if (player().can_place <= 0) {
            next();
            return;
        }

        setStartMenu();
        updateMiniMap();
    }

    private void gameOver() {
        // Game over;
        game_over = true;
        grid.render_pointer = false;
        setGameOverMenu();
    }

    private void updateMiniMap() {
        if (mini_map.spr.getTexture() != null) mini_map.spr.getTexture().dispose();
        mini_map.spr = new Sprite(new Texture(generateMiniMapPixmap()));
        mini_map.spr.setSize(core.w * 2, core.h * 2);
    }


    private Pixmap generateMiniMapPixmap() {
        Pixmap px = new Pixmap(core.w, core.h, Pixmap.Format.RGBA8888);
        Color corners = Color.valueOf("33333355");
        Color transparent = Color.valueOf("00000000");

        px.setColor(corners);
        px.fill();

        for (int x = 0; x < core.w; x++)
            for (int y = 0; y < core.h; y++) {
                Marker draw = core.get(x, core.h - y - 1);
                if (draw.owner == null)
                    px.setColor(transparent);
                else
                    px.setColor(Colors.getBG((Color) draw.owner));

                px.drawPixel(x, y);
            }

        return px;
    }

//    == == == == == == == == == == == ==
// == == == == == == == == == Menus == ==
//    == == == == == == == == == == == ==

    private void setStartMenu() {
        status.killall();
        if (!player().isAI)
            status.add(
                    new SelectableField() {
                        {
                            font_color = player().marker.cpy();
                            font = U.FONT_S;
                            color = Colors.getBG(player().marker.cpy());
                        }

                        @Override
                        public void onSelect(Vector2 mouse) {
                            setReturnMenu();
                        }

                        @Override
                        public String getText() {
                            return "To menu";
                        }
                    }
            );

        status.add(
                U.getTextField("Can place", U.FONT_S, player().marker),
                U.getTextField("" + player().can_place, U.FONT_H, player().marker)
        );

        if (player().isAI)
            status.add(U.getTextField("AI", U.FONT_S, player().marker));
    }

    private void setReturnMenu() {
        status.killall();
        status.add(U.getTextField("Are you sure?", U.FONT_S, player().marker));

        TableMenu sub = new TableMenu(2);
        sub.add(new SelectableField() {
            {
                font_color = player().marker.cpy();
                font = U.FONT_S;
                color = Colors.getBG(player().marker.cpy());
            }

            @Override
            public void onSelect(Vector2 mouse) {
                Luna.scene.changeScene(new MainMenu());
            }

            @Override
            public String getText() {
                return "yup";
            }
        });
        sub.add(new SelectableField() {
            {
                font_color = player().marker.cpy();
                font = U.FONT_S;
                color = Colors.getBG(player().marker.cpy());
            }

            @Override
            public void onSelect(Vector2 mouse) {
                setStartMenu();
            }

            @Override
            public String getText() {
                return "nope";
            }
        });
        sub.bg.setColor(0, 0, 0, 0);
        status.add(sub);
    }

    private void setGameOverMenu() {
        status.killall();
        status.add(
                TextField.create("game over", U.FONT_S),
                new SelectableField() {
                    {
                        font = U.FONT_S;
                    }

                    @Override
                    public void onSelect(Vector2 mouse) {
                        Luna.scene.changeScene(new ResultMenu(players, core));
                    }

                    @Override
                    public String getText() {

                        return "to results";
                    }
                }
        );
    }
}
