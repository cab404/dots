package com.cab404.dots.scene;

import com.cab404.defense.scene.Scene;
import com.cab404.defense.scene.SceneManager;
import com.cab404.dots.events.ResumeEvent;
import com.cab404.dots.render.ShapeRenderer;
import com.cab404.dots.utils.U;
import com.cab404.guidy.Luna;

/**
 * @author cab404
 */
public class FixedSceneManager extends SceneManager {

    public FixedSceneManager(Scene startScene) {
        super(startScene);
    }

    @Override
    public void resume() {
        super.resume();
        U.global.sendEvent(new ResumeEvent());
    }

    @Override
    public void render() {
        ShapeRenderer.getInstance().recreate();
        super.render();
    }
}
