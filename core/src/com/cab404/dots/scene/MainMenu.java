package com.cab404.dots.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.ui.DelimeterField;
import com.cab404.defense.ui.SelectableField;
import com.cab404.defense.ui.TextField;
import com.cab404.defense.util.FL;
import com.cab404.dots.events.EventListener;
import com.cab404.dots.events.ValueChangeEvent;
import com.cab404.dots.scene.menu.ValueField;
import com.cab404.dots.utils.Colors;
import com.cab404.dots.utils.U;
import com.cab404.guidy.Luna;

/**
 * @author cab404
 */
public class MainMenu extends MenuScene {

    private int player_count = 1;
    private int ai_count = 1;

    @Override
    public void create() {
        super.create();

        Luna.log("Started!");

//        Texture.setEnforcePotImages(false);

        // Initializing resources
        resume();

        gfx.cam.zoom = U.getHUDZoom();
        setMainMenu();
    }

    private void setMainMenu() {
        menu.killall();
        menu.add(
                TextField.create("Dots", U.FONT_H),
                TextField.create("version 1.1", U.FONT_S),
                new DelimeterField(0, 6),
                new SelectableField() {
                    {
                        font = U.FONT_S;
                    }

                    @Override
                    public void onSelect(Vector2 mouse) {
                        setNewGameMenu();
                    }

                    @Override
                    public String getText() {
                        return "Play";
                    }
                },
                new SelectableField() {
                    {
                        font = U.FONT_S;
                    }

                    @Override
                    public void onSelect(Vector2 mouse) {
                        // Here lies the rate mechanics.
                    }

                    @Override
                    public String getText() {
                        return "Rate";
                    }
                },
                new DelimeterField(0, 2)
        );
    }

    private void setNewGameMenu() {
        menu.killall();
        final float rnd = (float) Math.random();
        final SelectableField play_button = new SelectableField() {
            {
                font = U.FONT_S;
            }

            @Override
            public void onSelect(Vector2 mouse) {
                Luna.scene.changeScene(new GameScene(player_count, ai_count));
            }

            @Override
            public String getText() {
                return enabled ? ("Play" + (rnd > 0.3f ? rnd > 0.6f ? '?' : '!' : '.')) : "No players :(";
            }
        };

        menu.add(
                TextField.create("# of players:", U.FONT_S),
                new ValueField(
                        new Vector2(140, 20),
                        5,
                        player_count,
                        Color.WHITE.cpy(),
                        Colors.getBG(Color.DARK_GRAY.cpy()),
                        new EventListener() {
                            @Override
                            public void onEvent(Object object) {
                                if (object instanceof ValueChangeEvent) {
                                    player_count = ((ValueChangeEvent) object).new_value;
                                    play_button.enabled = (player_count + ai_count != 0);
                                }
                            }
                        }
                ),
                TextField.create("# of AIs:", U.FONT_S),
                new ValueField(
                        new Vector2(140, 20),
                        5,
                        ai_count,
                        Color.WHITE.cpy(),
                        Colors.getBG(Color.DARK_GRAY.cpy()),
                        new EventListener() {
                            @Override
                            public void onEvent(Object object) {
                                if (object instanceof ValueChangeEvent) {
                                    ai_count = ((ValueChangeEvent) object).new_value;
                                    play_button.enabled = (player_count + ai_count != 0);
                                }
                            }
                        }
                ),
                new DelimeterField(0, 3),
                play_button,
                new SelectableField() {
                    {
                        font = U.FONT_S;
                    }

                    @Override
                    public void onSelect(Vector2 mouse) {
                        setMainMenu();
                    }

                    @Override
                    public String getText() {
                        return "Nope, go back now!";
                    }
                }
        );

    }

    @Override
    public void resize(Vector2 size) {
        super.resize(size);
    }

    @SuppressWarnings("unused")
    public void resume() {
        FL.reset();
        FL.getInstance().reloadFont(Gdx.files.internal("data/font/0403.ttf"), U.FONT_S, 16);
        FL.getInstance().reloadFont(Gdx.files.internal("data/font/0403.ttf"), U.FONT_M, 24);
        FL.getInstance().reloadFont(Gdx.files.internal("data/font/0403.ttf"), U.FONT_H, 32);
    }

}
