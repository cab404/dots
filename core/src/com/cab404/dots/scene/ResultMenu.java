package com.cab404.dots.scene;

import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.ui.DelimeterField;
import com.cab404.defense.ui.SelectableField;
import com.cab404.dots.game.logic.Player;
import com.cab404.dots.game.processor.DotsProcessor;
import com.cab404.dots.utils.U;
import com.cab404.guidy.Luna;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Simple result screen
 *
 * @author cab404
 */
public class ResultMenu extends MenuScene {

    private class Result implements Comparable<Result> {
        final int score;
        final Player player;

        private Result(int score, Player player) {
            this.score = score;
            this.player = player;
        }

        @SuppressWarnings("NullableProblems")
        @Override
        public int compareTo(Result result) {
            return result.score - score;
        }
    }

    ArrayList<Result> results;

    public ResultMenu(ArrayList<Player> players, DotsProcessor core) {
        results = new ArrayList<>();

        for (Player player : players) {
            int score = 0;
            for (int x = 0; x < core.w; x++)
                for (int y = 0; y < core.h; y++)
                    if (player.marker.equals(core.get(x, y).owner))
                        score++;
            results.add(new Result(score, player));
        }

        Collections.sort(results);

    }

    @Override
    public void create() {
        super.create();
        gfx.cam.zoom = U.getHUDZoom();


        for (Result e : results)
            menu.add(U.getTextField(e.score + "", U.FONT_M, e.player.marker));

        menu.add(
                new DelimeterField(0, 4),
                new SelectableField() {
                    {
                        font = U.FONT_S;
                    }

                    @Override
                    public void onSelect(Vector2 mouse) {
                        Luna.scene.changeScene(new MainMenu());
                    }

                    @Override
                    public String getText() {
                        return "Get back to menu!";
                    }
                }
        );

    }
}
