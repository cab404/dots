package com.cab404.dots.scene.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.ui.Field;
import com.cab404.dots.events.EventListener;
import com.cab404.dots.events.EventPipe;
import com.cab404.dots.events.ValueChangeEvent;
import com.cab404.dots.utils.U;
import com.cab404.guidy.Luna;

/**
 * Simple value field
 *
 * @author cab404
 */
public class ValueField implements Field {

    private int max_value = 2;
    private int value = 1;
    private final EventPipe pipe;


    private Vector2 size = new Vector2(200, 32);
    private Color selected_color = Color.valueOf("FFFFFF");
    private Color unselected_color = Color.valueOf("222222");

    private Color zero_selected_color = Color.valueOf("FF0000");
    private Color zero_unselected_color = Color.valueOf("220000");

    public ValueField(Vector2 size, int max_value, int value, Color selected, Color unselected, EventListener... listeners) {
        this.size = size;
        this.value = value;
        this.max_value = max_value;
        this.selected_color = selected;
        this.unselected_color = unselected;

        pipe = new EventPipe();

        for (EventListener listener : listeners)
            pipe.addListener(listener);

    }

    @Override
    public void render(SpriteBatch batch, float phase, Vector2 lbc) {
        float s_width = size.x / max_value * phase;
        Vector2 cell = new Vector2(s_width - 1, size.y * phase);

        U.getSR().begin(batch);
        for (int i = 0; i < max_value; i++) {
            if (i > 0)
                U.getSR().setColor(i > value ? unselected_color : selected_color);
            else
                U.getSR().setColor(value != 0 ? zero_unselected_color : zero_selected_color);
//
            U.getSR().renderQuad(lbc.cpy().add(i * s_width, 0), cell);
        }
        U.getSR().end();
    }

    @Override
    public Vector2 getSize(float phase) {
        return size.cpy().scl(phase);
    }

    @Override
    public void onMouseOver(Vector2 mouse_pos) {
        if (Gdx.input.justTouched()) {

            int tmp_val = (int) (mouse_pos.x / size.x * max_value);

            if (tmp_val != value) {
                value = tmp_val;
                pipe.sendEvent(new ValueChangeEvent(tmp_val));
            }

        }
    }

    @Override
    public void update(float time) {

    }

    @Override
    public boolean isDead() {
        return false;
    }
}
