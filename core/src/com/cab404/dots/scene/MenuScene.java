package com.cab404.dots.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.Scene;
import com.cab404.defense.ui.ListMenu;
import com.cab404.defense.util.Graphics;
import com.cab404.guidy.Luna;

/**
 * @author cab404
 */
public class MenuScene implements Scene {
    protected Graphics gfx;
    protected ListMenu menu;

    @Override
    public void create() {
        gfx = new Graphics();
        menu = new ListMenu();
    }

    @Override
    public void update() {

        menu.update(Luna.dt());

        Vector2 mouse = gfx.convertToGFX(Luna.mouse(0));
        if (menu.getBounds().contains(mouse.x, mouse.y))
            menu.onMouseOver(mouse.cpy().sub(menu.pos));

        gfx.update();
        menu.pos.set(gfx.cam_size().sub(menu.size).scl(0.5f));
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gfx.batch.begin();
        menu.render(gfx.batch);
        gfx.batch.end();
    }

    @Override
    public void dispose() {
    }

    @Override
    public void resize(Vector2 size) {
    }
}
