package com.cab404.dots.events;

import java.util.ArrayList;

/**
 * Простая труба для отправки событий.
 *
 * @author cab404
 */
public class EventPipe {

    private final ArrayList<EventListener> listeners;

    public EventPipe() {
        listeners = new ArrayList<>();
    }

    public void addListener(EventListener listener) {
        listeners.add(listener);
    }

    public void removeListener(EventListener listener) {
        listeners.remove(listener);
    }

    public void sendEvent(Object event) {
        for (EventListener listener : listeners) {
            listener.onEvent(event);
        }
    }

//    public static EventListener getFwd(final EventPipe redirect_to) {
//        return new EventListener() {
//            @Override
//            public void onEvent(Object object) {
//                redirect_to.sendEvent(object);
//            }
//        };
//    }

}
