package com.cab404.dots.events;

/**
 * Little event handling interface
 *
 * @see LinkageEvent
 * @see OwnerChangeEvent
 */
public interface EventListener {
    public void onEvent(Object object);
}
