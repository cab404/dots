package com.cab404.dots.events;

import com.cab404.dots.game.processor.Link;

/**
 * Occurs then new link between cells created. Usually used by renderer.
 */
public class LinkageEvent {
    public final Link link;

    public LinkageEvent(Link link) {
        this.link = link;
    }
}
