package com.cab404.dots.events;

/**
 * @author cab404
 */
public class ValueChangeEvent {
    public final int new_value;

    public ValueChangeEvent(int new_value) {
        this.new_value = new_value;
    }
}
