package com.cab404.dots.events;

/**
 * @author cab404
 */
public class MoveEvent {
    public final int x, y;

    public MoveEvent(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
