package com.cab404.dots.events;

import com.cab404.dots.game.processor.Marker;

/**
 * Occurs then cell changes owner.
 */
public class OwnerChangeEvent {

    /**
     * Cell in it's unchanged state
     */
    public final Marker field;

    /**
     * Pending owner.
     */
    public final Object new_owner;

    /**
     * Recursion level of this changes.
     * Made just for renderer.
     */
    public final int recursion_level;


    public OwnerChangeEvent(Marker field, Object new_owner, int recursion_level) {
        this.field = field;
        this.new_owner = new_owner;
        this.recursion_level = recursion_level;
    }
}
