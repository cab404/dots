package com.cab404.dots.utils;

import com.cab404.defense.objects.GameObj;
import com.cab404.defense.storage.AbstractObjectStorage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * @author cab404
 */
public class SimpleObjectStorage extends AbstractObjectStorage {
    ArrayList<GameObj> objects;

    public SimpleObjectStorage() {
        objects = new ArrayList<>();
    }

    @Override
    public void add(GameObj... add) {
        super.add(add);
        objects.addAll(Arrays.asList(add));
    }

    @Override
    public void remove(GameObj... remove) {
        objects.removeAll(Arrays.asList(remove));
    }

    @Override
    public void update(float time) {
        super.update(time);
    }

    @Override
    public Collection<GameObj> getAll() {
        return objects;
    }
}
