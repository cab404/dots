package com.cab404.dots.utils;

import com.badlogic.gdx.graphics.Color;

/**
 * Animation parameters
 *
 * @author cab404
 */
public class A {

    private static float ds = U.getHUDZoom();
    public static float cell_size = 20 / ds;
    public static float connection_thickness = 5 / ds;
    public static float outpost_size = 10 / ds;

    public static float delay_animation = 0.05f;
    public static float animation_length = 0.25f;

    public static boolean render_grid = true;
    public static Color grid_color = Color.valueOf("222222");
    public static float grid_thickness = 2f;

    public static int frames_between_ai_moves = 10;

    public static int particles_at_side = 3;

    public static Color pointer_color = Color.valueOf("444444");

}
