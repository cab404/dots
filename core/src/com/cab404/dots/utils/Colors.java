package com.cab404.dots.utils;

import com.badlogic.gdx.graphics.Color;

/**
 * @author cab404
 */
public class Colors {
    public static Color[] colors = {
            Color.valueOf("ef5350"),
            Color.valueOf("ab47bc"),
            Color.valueOf("3f51b5"),
            Color.valueOf("03a9f4"),
            Color.valueOf("009688"),
            Color.valueOf("8bc34a"),
            Color.valueOf("ffeb3b"),
            Color.valueOf("ff9800"),
    };

    public static Color getBG(Color modify) {
        Color cpy = modify.cpy();
        cpy.a = 0.5f;
        return cpy;
    }

    public static Color getSolidBG(Color modify) {
        Color cpy = modify.cpy();
        cpy.mul(0.5f);
        cpy.a = 1f;
        return cpy;
    }

    public static Color invert(Color modify) {
        Color cpy = modify.cpy();
        cpy.r = 1f - cpy.r;
        cpy.g = 1f - cpy.g;
        cpy.b = 1f - cpy.b;
        return cpy;
    }

}
