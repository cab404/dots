package com.cab404.dots.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.cab404.defense.scene.Scene;
import com.cab404.defense.ui.TextField;
import com.cab404.dots.events.EventListener;
import com.cab404.dots.events.EventPipe;
import com.cab404.dots.events.ResumeEvent;
import com.cab404.dots.render.ShapeRenderer;
import com.cab404.guidy.Luna;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Utilities
 *
 * @author cab404
 */
public class U {

    public static final int FONT_S = 22;
    public static final int FONT_M = 23;
    public static final int FONT_H = 24;

    public static final EventPipe global = new EventPipe();

    static {
        // Handles resume action
        global.addListener(new EventListener() {
            @Override
            public void onEvent(Object object) {
                if (object instanceof ResumeEvent) {
                    Scene currentScene = Luna.scene.getCurrentScene();
                    try {
                        Method method = currentScene.getClass().getMethod("resume");
                        method.invoke(currentScene);
                    } catch (NoSuchMethodException e) {
                        //
                    } catch (InvocationTargetException e) {
                        //
                    } catch (IllegalAccessException e) {
                        //
                    }
                }
            }
        });
    }

    public static float getHUDZoom() {
        return getHUDZoom(2);
    }

    public static float getHUDZoom(float scale) {
        return 1 / (Gdx.graphics.getDensity() * scale);
    }

    public static ShapeRenderer getSR() {
        return ShapeRenderer.getInstance();
    }

    public static TextField getTextField(final String text, final int font_, final Color col) {
        return new TextField() {
            {
                this.color = Colors.getBG(col);
                this.font_color = col;
                this.font = font_;

            }

            @Override
            public String getText() {
                return text;
            }
        };
    }

}
