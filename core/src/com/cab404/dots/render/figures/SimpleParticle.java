package com.cab404.dots.render.figures;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.dots.utils.A;
import com.cab404.dots.utils.U;

/**
 * @author cab404
 */
public class SimpleParticle implements Particle {
    private final Vector2 pos, speed;
    private boolean isAlive = true;
    private Color color;
    private float time, start_time;

    @Override
    public boolean isAlive() {
        return isAlive;
    }

    public SimpleParticle(Vector2 pos, Vector2 speed, Color color) {
        this.pos = pos;
        this.color = color;
        this.speed = speed;

        start_time = time = (float) ((Math.random() * 0.5) + 1);
    }

    @Override
    public void render(SpriteBatch batch, Vector2 pos) {

        float cs = A.cell_size / A.particles_at_side;
        Vector2 side = new Vector2(cs, cs).scl(0.5f);

        U.getSR().setColor(color);
        U.getSR().renderRectangle(this.pos.cpy().add(pos).add(side), cs);

    }

    @Override
    public void update(float time) {
        this.time -= time;

        if (this.time < 0) {
            isAlive = false;
            return;
        }

        pos.add(speed.cpy().scl(time));
        color.a = this.time / start_time;

    }
}

