package com.cab404.dots.render.figures;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

/**
 * @author cab404
 */
public interface Primitive {
    public void render(SpriteBatch batch, Vector2 pos);

    public void update(float time);
}
