package com.cab404.dots.render.figures;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.dots.game.processor.Link;
import com.cab404.dots.utils.A;
import com.cab404.dots.utils.U;

/**
 * @author cab404
 */
public class Line implements Primitive {
    Vector2 start, finish;
    Color color;
    float started = 1f, delay = A.animation_length;

    public Line(Link link) {
        start = new Vector2(link.x1, link.y1).add(0.5f, 0.5f).scl(A.cell_size);
        finish = new Vector2(link.x2, link.y2).add(0.5f, 0.5f).scl(A.cell_size);
        color = (Color) link.creator;
    }


    @Override
    public void render(SpriteBatch batch, Vector2 pos) {
        U.getSR().setColor(color);
        U.getSR().renderLine(start.cpy().add(pos), finish.cpy().add(pos), A.connection_thickness);
    }

    @Override
    public void update(float time) {
        started -= time / delay;
        started = started > 0 ? started < 1 ? started : 1 : 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Line)) return false;
        Line o2 = (Line) obj;
        return ((start.equals(o2.start) && finish.equals(o2.finish)) || (start.equals(o2.finish) && finish.equals(o2.start)));
    }

    @Override
    public int hashCode() {
        return start.hashCode() & finish.hashCode();
    }
}
