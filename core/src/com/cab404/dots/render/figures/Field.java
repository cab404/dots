package com.cab404.dots.render.figures;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.dots.events.OwnerChangeEvent;
import com.cab404.dots.utils.A;
import com.cab404.dots.utils.Colors;
import com.cab404.dots.utils.U;

/**
 * @author cab404
 */
public class Field implements Primitive {
    int x, y;
    Color color;
    float started = 1f, delay = A.animation_length;

    public Field(OwnerChangeEvent e) {
        x = e.field.x;
        y = e.field.y;
        color = Colors.getSolidBG((Color) e.new_owner);
    }

    @Override
    public void render(SpriteBatch batch, Vector2 pos) {
        Vector2 coord =
                new Vector2(A.cell_size, A.cell_size).scl(.5f)
                        .add(
                                new Vector2(A.cell_size, A.cell_size)
                                        .scl(x, y)
                        )
                        .add(pos);

        U.getSR().setColor(color);
        U.getSR().renderRectangle(coord, A.cell_size * (1f - started));
    }

    @Override
    public void update(float time) {
        started -= time / delay;
        started = started > 0 ? started < 1 ? started : 1 : 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Field field = (Field) o;

        return x == field.x && y == field.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
