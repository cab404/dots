package com.cab404.dots.render.figures;

/**
 * @author cab404
 */
public interface Particle extends Primitive {
    public boolean isAlive();
}
