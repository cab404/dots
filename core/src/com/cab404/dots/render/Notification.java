package com.cab404.dots.render;

import com.badlogic.gdx.graphics.Color;
import com.cab404.defense.ui.TextField;
import com.cab404.dots.utils.Colors;
import com.cab404.dots.utils.U;

/**
 * @author cab404
 */
public class Notification extends TextField {

    private String str;
    float remaining = 5f;

    public Notification(String str, Color col) {
        this.color.set(Colors.getBG(col));
        this.font_color.set(col);
        this.font = U.FONT_S;
        this.str = str;
    }

    @Override
    public String getText() {
        return str;
    }

    @Override
    public boolean isDead() {
        return remaining < 0;
    }

    @Override
    public void update(float time) {
        super.update(time);
        remaining -= time;
    }
}
