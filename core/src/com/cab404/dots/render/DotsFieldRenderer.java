package com.cab404.dots.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import com.cab404.dots.events.*;
import com.cab404.dots.game.processor.DotsProcessor;
import com.cab404.dots.render.figures.*;
import com.cab404.dots.utils.A;
import com.cab404.dots.utils.U;
import com.cab404.guidy.Luna;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

/**
 * @author cab404
 */
public class DotsFieldRenderer extends GameObj {

    public final EventPipe pipe;
    private DotsProcessor core;
    private Sound sound;


    private ArrayList<AnimationQueryObject> query;
    private ArrayList<Particle> particles;
    private HashSet<Outpost> outposts;
    private ArrayList<Line> links;
    private HashSet<Field> fields;

    public boolean render_pointer = true;


    public DotsFieldRenderer(DotsProcessor core) {
        sound = Gdx.audio.newSound(Gdx.files.internal("sounds/pshh.ogg"));

        core.addListener(
                new EventListener() {
                    @Override
                    public void onEvent(Object object) {
                        if (object instanceof OwnerChangeEvent) {
                            OwnerChangeEvent e = (OwnerChangeEvent) object;

                            if (e.field.creator != null)
                                query.add(new AnimationQueryObject(new Outpost(e), e.recursion_level * A.delay_animation));

                            // Throw out some particles
                            if (e.field.owner != null) {
                                query.add(new AnimationQueryObject(new SoundParticle(), e.recursion_level * A.delay_animation));

                                float side = A.cell_size / A.particles_at_side;

                                Vector2 c_pos = new Vector2(e.field.x, e.field.y).scl(A.cell_size);
                                Vector2 c_side = new Vector2(side, side);

                                for (int x = 0; x < A.particles_at_side; x++)
                                    for (int y = 0; y < A.particles_at_side; y++) {
                                        query.add(
                                                new AnimationQueryObject(
                                                        new SimpleParticle(
                                                                c_pos.cpy().add(c_side.cpy().scl(x, y)),
                                                                new Vector2(
                                                                        (float) (-(A.particles_at_side / 2 - x) * Math.random() * 50),
                                                                        (float) (-(A.particles_at_side / 2 - y) * Math.random() * 50)
                                                                ),
                                                                ((Color) e.field.owner).cpy()
                                                        ),
                                                        e.recursion_level * A.delay_animation
                                                )
                                        );
                                    }
                            }

                            query.add(new AnimationQueryObject(new Field(e), e.recursion_level * A.delay_animation));
                        }
                        if (object instanceof LinkageEvent) {
                            Line line = new Line(((LinkageEvent) object).link);

                            if (links.contains(line))
                                links.remove(line);
                            links.add(line);

                        }
                    }
                }
        );

        fields = new HashSet<>();
        links = new ArrayList<>();
        outposts = new HashSet<>();
        particles = new ArrayList<>();

        query = new ArrayList<>();
        this.core = core;

        size = new Vector2(core.w * A.cell_size, core.h * A.cell_size);

        pipe = new EventPipe();
    }

    @Override
    public void render(SpriteBatch batch) {
        U.getSR().begin(batch);

        U.getSR().setColor(A.grid_color);
        Vector2 xed = new Vector2(A.cell_size, 0);
        Vector2 yed = new Vector2(0, A.cell_size);

        if (A.render_grid) {
            for (int x = 0; x <= core.w; x++)
                U.getSR().renderLine(
                        xed.cpy().scl(x).sub(0, A.grid_thickness / 2).add(pos),
                        xed.cpy().scl(x).add(yed.cpy().scl(core.h)).add(0, A.grid_thickness / 2).add(pos),
                        A.grid_thickness);

            for (int y = 0; y <= core.w; y++)
                U.getSR().renderLine(
                        yed.cpy().scl(y).sub(A.grid_thickness / 2, 0).add(pos),
                        yed.cpy().scl(y).add(xed.cpy().scl(core.w)).add(A.grid_thickness / 2, 0).add(pos),
                        A.grid_thickness);
        }

        if (render_pointer) {
            U.getSR().setColor(A.grid_color);
            if (cx != -1)
                U.getSR().renderRectangle(
                        pos.cpy().add(
                                new Vector2(A.cell_size, A.cell_size)
                                        .scl(cx, cy)
                        )
                                .add(
                                        A.cell_size / 2, A.cell_size / 2
                                ),
                        A.cell_size);
        }

        for (Field field : fields)
            field.render(batch, pos);
        for (Line line : links)
            line.render(batch, pos);
        for (Outpost outpost : outposts)
            outpost.render(batch, pos);
        for (Particle particle : particles)
            particle.render(batch, pos);

        U.getSR().end();


    }

//    float logDelay = 0;

//    private void logInfo(float time) {
//        logDelay += time;
//        if (logDelay > 10f) {
//            logDelay = 0;
//            Luna.log("/*=*=*=*=*=*=*/");
//            Luna.log("Outpost:" + outposts.size());
//            Luna.log("Link:" + links.size());
//            Luna.log("Field:" + fields.size());
//            Luna.log("Particles:" + particles.size());
//        }
//    }


    @SuppressWarnings("unchecked")
    @Override
    public void update(float time) {
//        logInfo(time);

        for (Outpost outpost : outposts)
            outpost.update(time);
        for (Line line : links)
            line.update(time);
        for (Field field : fields)
            field.update(time);

        for (int i = 0; i < particles.size(); i++) {
            Particle test = particles.get(i);
            if (test.isAlive())
                test.update(time);
            else
                particles.remove(i);
        }


        ArrayList<AnimationQueryObject> to_rm = new ArrayList<>();
        for (AnimationQueryObject obj : query) {
            obj.duration -= time;
            if (obj.duration < 0) {

                Collection<? extends Primitive> c = null;
                if (obj.queried instanceof Particle) c = particles;
                if (obj.queried instanceof Outpost) c = outposts;
                if (obj.queried instanceof Field) c = fields;
                if (obj.queried instanceof Line) c = links;
                if (c == null) throw new NullPointerException();

                c.remove(obj.queried);
                ((Collection<Primitive>) c).add(obj.queried);
                to_rm.add(obj);
            }
        }
        query.removeAll(to_rm);
    }

    private int cx = -1, cy = -1;
    Vector2 offset = null;

    @Override
    public void onMouseOver(Vector2 mousePos) {
        mousePos = Luna.mouse(0);
        mousePos = parent.gfx.convertToGFX(mousePos);
        Vector2 cellMousePos = mousePos.cpy().sub(pos).scl(1f / A.cell_size);

        int tmp_cx = (int) Math.floor(cellMousePos.x);
        int tmp_cy = (int) Math.floor(cellMousePos.y);


        if (Gdx.input.isTouched()) {

            if (offset != null) {
                pos.add(mousePos.cpy().sub(offset));

                Rectangle cam = parent.gfx.getCameraBounds();
                Rectangle slf = getBounds();

                if (slf.x > cam.x) slf.x = cam.x;
                if (slf.y > cam.y) slf.y = cam.y;

                if (slf.x + slf.width < cam.x + cam.width) slf.x = cam.x + cam.width - slf.width;
                if (slf.y + slf.height < cam.y + cam.height) slf.y = cam.y + cam.height - slf.height;

                pos.set(slf.x, slf.y);

                offset = mousePos.cpy();
            }

            if (Gdx.input.justTouched())
                if (cx == tmp_cx && cy == tmp_cy) {
                    pipe.sendEvent(new MoveEvent(cx, cy));
                    cx = -1;
                    cy = -1;
                } else {
                    if (core.get(tmp_cx, tmp_cy).owner == null) {
                        cx = tmp_cx;
                        cy = tmp_cy;
                    }
                    offset = mousePos.cpy();
                }
        } else
            offset = null;

    }

    private class AnimationQueryObject {
        Primitive queried;
        float duration;

        private AnimationQueryObject(Primitive queried, float duration) {
            this.queried = queried;
            this.duration = duration;
        }

    }

    private class SoundParticle implements Particle {

        boolean soundPlayed = false;

        @Override
        public void render(SpriteBatch batch, Vector2 pos) {
            sound.play(0.1f);
            soundPlayed = true;
        }

        @Override
        public void update(float time) {
        }

        @Override
        public boolean isAlive() {
            return !soundPlayed;
        }
    }


}
