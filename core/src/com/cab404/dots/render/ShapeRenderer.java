package com.cab404.dots.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.dots.events.EventListener;
import com.cab404.dots.events.ResumeEvent;
import com.cab404.dots.utils.U;
import com.cab404.guidy.Luna;

/**
 * @author cab404
 */
public class ShapeRenderer {

    Sprite pix;

    private static ShapeRenderer SINGLETON;
    private com.badlogic.gdx.graphics.glutils.ShapeRenderer under = new com.badlogic.gdx.graphics.glutils.ShapeRenderer();


    public void recreate() {
        SINGLETON = new ShapeRenderer();
    }

    public static ShapeRenderer getInstance() {
        if (SINGLETON == null) {
            SINGLETON = new ShapeRenderer();
        }
        return SINGLETON;
    }


    private ShapeRenderer() {

//        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
//        pixmap.setColor(Color.WHITE);
//        pixmap.drawPixel(0, 0);
//        pix = new Sprite(new Texture(pixmap));
    }


    public void renderLine(Vector2 a, Vector2 b, float thick) {
        under.rectLine(a, b, thick);
//        Vector2 vec = a.cpy().sub(b);
//
//        pix.setSize(thick, a.dst(b));
//        pix.setPosition(a.x - thick / 2, a.y);
//        pix.setOrigin(thick / 2, 0);
//        pix.setRotation(vec.cpy().angle() + 90);
//        pix.draw(batch);
//        pix.setRotation(0);
//        pix.setOrigin(0, 0);

    }

    public void setColor(Color color) {
//        if (color == null) {
//            under.setColor(new Color(0, 0, 0, 0));
//            return;
//        }
        under.setColor(color);
    }

    public void renderRectangle(Vector2 centre, float side) {
        under.rect(centre.x - side / 2, centre.y - side / 2, side, side);
//
//        pix.setSize(side, side);
//        centre = centre.cpy().sub(side / 2, side / 2);
//        pix.setPosition(centre.x, centre.y);
//
//        pix.draw(batch);
    }

    SpriteBatch cbatch;
    public void begin(SpriteBatch batch) {
        cbatch = batch;
        batch.end();
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        under.setTransformMatrix(batch.getTransformMatrix());
        under.setProjectionMatrix(batch.getProjectionMatrix());
        under.begin(com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType.Filled);
    }

    public void end() {
        under.end();
        cbatch.begin();
        cbatch = null;
    }

    public void renderQuad(Vector2 lbc, Vector2 size) {
        under.rect(lbc.x, lbc.y, size.x, size.y);

//        pix.setSize(size.x, size.y);
//        pix.setPosition(lbc.x, lbc.y);
//
//        pix.draw(batch);
    }

}
