package com.cab404.dots.render;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.cab404.defense.objects.GameObj;

/**
 * @author cab404
 */
public class Image extends GameObj {
    public Sprite spr;

//    public Image(Texture tx) {
//        super();
//        spr = new Sprite(tx);
//    }

    public Image(Sprite spr) {
        super();
        this.spr = spr;
    }

    @Override
    public void render(SpriteBatch batch) {
        spr.setPosition(pos.x, pos.y);
        spr.draw(batch);
    }

    @Override
    public void update(float time) {
    }

    @Override
    public Rectangle getBounds() {
        return spr.getBoundingRectangle();
    }
}
