package com.cab404.dots.game.processor;

import com.cab404.dots.events.EventPipe;
import com.cab404.dots.events.LinkageEvent;
import com.cab404.dots.events.OwnerChangeEvent;

import java.util.*;

/**
 * Игровой процессор "Точек"
 *
 * @author cab404
 */
@SuppressWarnings("UnusedDeclaration")
public class DotsProcessor implements Iterable<Marker> {
    /**
     * Игровое поле.
     */
    public final Marker[][] field;
    /**
     * Ширина.
     */
    public final int w;
    /**
     * Высота.
     */
    public final int h;
    private Set<Link> links;

    private EventPipe pipe;

    private void sendEvent(Object event) {
        pipe.sendEvent(event);
    }

    /**
     * Создаёт поле по высоте и ширине.
     */
    public DotsProcessor(int w, int h) {
        this.w = w;
        this.h = h;

        field = new Marker[w][h];
        links = new HashSet<>();
        pipe = new EventPipe();

        for (int x = 0; x < w; x++)
            for (int y = 0; y < h; y++)
                field[x][y] = new Marker(x, y);

    }

    /**
     * Пытается занять заданный маркер заданным игроком.
     */
    public boolean move(Object player, int x, int y) {
        if (get(x, y).creator == null && get(x, y).owner == null) {
            get(x, y).creator = player;
            sendEvent(new OwnerChangeEvent(get(x, y), player, 0));
            get(x, y).owner = player;
            complete(x, y);
            return true;
        } else
            return false;
    }

    /**
     * 000
     * 0+0
     * 000
     */
    public List<Marker> getCompoundCells(int x, int y) {
        List<Marker> out = new ArrayList<>();

        // Eeyup, something like this.
        for (int dx = -1; dx <= 1; dx++)
            for (int dy = -1; dy <= 1; dy++)
                if (dx != 0 || dy != 0)
                    if (x + dx < w && x + dx >= 0)
                        if (y + dy < w && y + dy >= 0)
                            out.add(field[x + dx][y + dy]);

        return out;

    }

    /**
     * -0-
     * 0+0
     * -0-
     */
    public List<Marker> getFullyCompoundCells(int x, int y) {
        List<Marker> out = new ArrayList<>();

        // Eeyup, something like this.
        for (int dx = -1; dx <= 1; dx++)
            for (int dy = -1; dy <= 1; dy++)
                if (dx == 0 ^ dy == 0)
                    if (x + dx < w && x + dx >= 0)
                        if (y + dy < w && y + dy >= 0)
                            out.add(field[x + dx][y + dy]);

        return out;

    }

    /**
     * Creates a link path from a marker set
     */
    private List<Link> getLinks(Set<Marker> raw) {
        List<List<Marker>> paths = new ArrayList<>();
        List<Link> links = new ArrayList<>();

        for (Marker a : raw)
            for (Marker b : raw) {
                if (a.isCompoundTo(b)) {
                    links.add(new Link(a, b));
                }
            }

        return links;
    }

    /**
     * Returns all current links on board.
     */
    public Set<Link> getLinks() {
        return Collections.unmodifiableSet(links);
    }

    /**
     * Returns cell with supplied coordinates or null if it is out of bounds
     */
    public Marker get(int x, int y) {
        if (x >= 0 && x < w && y >= 0 && y < h)
            return field[x][y];
        else return null;
    }

    public void removeListener(com.cab404.dots.events.EventListener listener) {
        pipe.removeListener(listener);
    }

    public void addListener(com.cab404.dots.events.EventListener eventListener) {
        pipe.addListener(eventListener);
    }


    /* Here goes non-recursive Android-and-10k*10k-friendly part */

    /**
     * Checks for captures without recursion
     */
    private void complete(int x, int y) {

        Object owner = get(x, y).owner;

        for (Marker marker : getFullyCompoundCells(x, y))
            if (marker.owner == null || !marker.owner.equals(owner)) {
                HashMap<Marker, Integer> indexed_area = new HashMap<>();

                if (findInnerNoRecursive(owner, marker.x, marker.y, indexed_area)) {
                    indexed_area.put(get(marker.x, marker.y), 0);
                    HashSet<Marker> boundaries = new HashSet<>();
                    getBoundsAndFillAreaWithYourPresenceWithoutRecursion(owner, indexed_area, boundaries);

                    List<Link> links = getLinks(boundaries);
                    for (Link link : links)
                        sendEvent(new LinkageEvent(link));
                    this.links.addAll(links);

                }

            }

    }

    /**
     * Fills enclosed figure (and IMHO&lt;cab404&gt; result looks prettier then recursive version)
     */
    private void getBoundsAndFillAreaWithYourPresenceWithoutRecursion(Object owner, HashMap<Marker, Integer> iterated, Set<Marker> boundaries) {

        for (Map.Entry<Marker, Integer> e : iterated.entrySet()) {

            sendEvent(new OwnerChangeEvent(get(e.getKey().x, e.getKey().y), owner, e.getValue()));
            get(e.getKey().x, e.getKey().y).owner = owner;

            List<Marker> compoundCells = getFullyCompoundCells(e.getKey().x, e.getKey().y);

            for (Marker cell : compoundCells) {
                if (!iterated.keySet().contains(cell)) {
                    boundaries.add(cell);
                }
            }

        }
    }

    /**
     * What if I had this cell? Or what if I leave this cell for enemies to capture? All in one number.
     */
    public int findMoveWeight(Object owner, Marker cell) {
        int metascore = 0;

        if (cell.owner != null) {
            return 0;
        }

        int compound_allies = 0;

        HashMap<Object, Integer> pretenders = new HashMap<>();

        for (Marker compound : getCompoundCells(cell.x, cell.y)) {
            if (owner.equals(compound.owner))
                compound_allies++;
            else if (compound.owner != null) {

                if (pretenders.containsKey(compound.owner))
                    pretenders.put(compound.owner, pretenders.get(compound.owner) + 1);
                else
                    pretenders.put(compound.owner, 1);

            }
        }


        cell.owner = owner;
        if (compound_allies > 1)
            for (Marker marker : getFullyCompoundCells(cell.x, cell.y)) {
                Map<Marker, Integer> area = new HashMap<>();
                if (findInnerNoRecursive(owner, marker.x, marker.y, area)) {
                    area.put(marker, 0);

                    metascore += area.keySet().size();
                    for (Marker check : area.keySet()) {
                        if (check.owner != null) metascore += 2;
                    }

                }
            }


        for (Map.Entry<Object, Integer> e : pretenders.entrySet()) {
            if (e.getValue() > 1) {
                cell.owner = e.getKey();
                for (Marker marker : getFullyCompoundCells(cell.x, cell.y)) {

                    Map<Marker, Integer> area = new HashMap<>();
                    if (findInnerNoRecursive(e.getKey(), marker.x, marker.y, area)) {
                        area.put(marker, 0);

                        for (Marker check : area.keySet()) {
                            if (owner.equals(check.owner)) {
                                metascore++;
                            }
                        }

                    }
                }
            }
        }

        cell.owner = null;


        return metascore;

    }

    @Override
    public Iterator<Marker> iterator() {
        return new Iterator<Marker>() {
            private int x;
            private int y;

            @Override
            public boolean hasNext() {
                return y < h - 1;
            }

            @Override
            public Marker next() {
                x++;
                if (x == w) {
                    y++;
                    x = 0;
                }
                return get(x, y);
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }


    private class FieldVector {
        final Marker start;
        final int dx, dy;

        /**
         * Iteration when that vector was created. Useful for animation purposes.
         */
        final int itr;

        private FieldVector(Marker start, int itr, int dx, int dy) {
            this.start = start;
            this.dx = dx;
            this.dy = dy;
            this.itr = itr;
        }

    }

    /**
     * Creates and inserts field vectors into supplied array.
     */
    private void putAllVectors(Collection<FieldVector> vectors, int itr, Marker marker, MarkerChecker checker) {

        FieldVector[] vct = {
                new FieldVector(marker, itr, -1, 0),
                new FieldVector(marker, itr, +1, 0),
                new FieldVector(marker, itr, 0, -1),
                new FieldVector(marker, itr, 0, +1)
        };

        for (FieldVector vector : vct) {
            Marker test = get(marker.x + vector.dx, marker.y + vector.dy);
            if (test != null && checker.check(test))
                vectors.add(vector);
        }
        vectors.addAll(Arrays.asList(vct));

    }


    /**
     * Non-recursive version of findInner. That thing took a lot of time.
     */
    public boolean findInnerNoRecursive(final Object owner, int x, int y, Map<Marker, Integer> iterated) {

        return getSector(get(x, y), iterated, new MarkerChecker() {
            @Override public boolean check(Marker marker) {
                return !owner.equals(marker.owner);
            }
        });

    }


    /**
     * Returns all boundaries of the map sectors which can be captured.
     */
    public List<Set<Marker>> getSectors(final Object point_of_view) {
        ArrayList<Set<Marker>> areas = new ArrayList<>();

        area_search:
        for (Marker check : this) {
            if (check.owner == null || check.owner.equals(point_of_view)) continue;

            for (Set<Marker> area : areas) {
                if (area.contains(check))
                    continue area_search;
            }

            Map<Marker, Integer> itr = new HashMap<>();

            getSector(check, itr, new MarkerChecker() {
                @Override public boolean check(Marker marker) {
                    return (marker.owner != null && !point_of_view.equals(marker.owner));
                }
            });

            areas.add(new HashSet<>(itr.keySet()));

        }

        return areas;

    }

    public Set<Marker> getBounds(Set<Marker> area) {
        HashSet<Marker> output = new HashSet<>();

        for (Marker check : area)
            for (Marker p_bound : getFullyCompoundCells(check.x, check.y))
                if (!area.contains(p_bound))
                    output.add(p_bound);

        return output;
    }

    public interface MarkerChecker {
        public boolean check(Marker marker);
    }

    /**
     * Returns all cells of supplied command
     */
    public int friends(Marker whom, Object owner) {
        int ret = 0;
        for (Marker maybe_friend : getFullyCompoundCells(whom.x, whom.y))
            if (owner.equals(maybe_friend.owner))
                ret++;
        return ret;
    }

    public Set<Object> enemies(Marker whom, Object owner) {
        HashSet<Object> enemies = new HashSet<>();

        for (Marker neighbour : getCompoundCells(whom.x, whom.y))
            if (neighbour.owner != null && !neighbour.owner.equals(owner))
                enemies.add(neighbour.owner);

        return enemies;
    }

    /**
     * Returns enclosed sector in start location using supplied inclusion check function. Performance bottleneck.
     *
     * @param start    Start location
     * @param checker  Checker function. Checks if supplied marker can be added to the area.
     * @param iterated Output data storage. Contains all data and iteration of it's inclusion.
     */
    public boolean getSector(Marker start, Map<Marker, Integer> iterated, MarkerChecker checker) {
//      Recursion data.
        HashSet<Marker> area = new HashSet<>();
        iterated.put(start, 0);

//      Entry points for algorithm start-up.
        LinkedList<FieldVector> can_switch_to = new LinkedList<>();

//      Initializing vector array, so we have start-up data.
        putAllVectors(can_switch_to, 0, start, checker);

//      We'll continue iterating till we have entry points. Just a simple recursion simulation.
        while (!can_switch_to.isEmpty()) {

            FieldVector path_data = can_switch_to.pop();

            Marker pending = path_data.start;
            int dx = path_data.dx, dy = path_data.dy, cur_itr = path_data.itr;

//          Checking if we are on map bound
            if (getFullyCompoundCells(pending.x, pending.y).size() < 4) return false;

//          Testing for vector pointer destination validity
            Marker potential_next = get(pending.x + dx, pending.y + dy);

//          Testing for cell validity
            if (!checker.check(potential_next) || area.contains(potential_next))
                continue;

//          Moving forwards using supplied vector till we have no checker passing point
            while (true) {
                cur_itr++;
                area.add(pending);

                iterated.put(pending, cur_itr);

                Marker next = get(pending.x + dx, pending.y + dy);
                if (getFullyCompoundCells(pending.x, pending.y).size() < 4) return false;

                // Adding new entry points. This is a part, where optimisation should be done.
                putAllVectors(can_switch_to, cur_itr, pending, checker);

                if (next == null)
                    return false;

                if (checker.check(next) && !area.contains(next))
                    pending = next;
                else break;
            }

        }

        return true;
    }


}
