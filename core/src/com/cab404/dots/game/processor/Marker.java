package com.cab404.dots.game.processor;

/**
 * @author cab404
 */
public class Marker {
    public Object creator;
    public Object owner;
    public final int x;
    public final int y;

    public Marker(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean isCompoundTo(Marker to) {
        int dx = Math.abs(x - to.x);
        int dy = Math.abs(y - to.y);
        return ((dx <= 1 && dx >= 0) && (dy <= 1 && dy >= 0) && (dy != 0 || dx != 0));
    }

    @Override
    public String toString() {
        return x + ":" + y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Marker marker = (Marker) o;

        return x == marker.x && y == marker.y;

    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
