package com.cab404.dots.game.processor;

import java.util.Arrays;

/**
 * @author cab404
 */
public class Link {
    public final Object creator;
    public final int x1, y1, x2, y2;

    public Link(Marker o1, Marker o2) {
        x1 = o1.x;
        y1 = o1.y;
        x2 = o2.x;
        y2 = o2.y;
        creator = (o1.creator != null && o1.creator.equals(o2.creator)) ? o1.creator : null;
    }

    @Override
    public boolean equals(Object obj) {
        return ((obj instanceof Link))
                &&
                (
                        (
                                x1 == ((Link) obj).x1 &&
                                        y1 == ((Link) obj).y1 &&
                                        x2 == ((Link) obj).x2 &&
                                        y2 == ((Link) obj).y2
                        )
                                ||
                                (x1 == ((Link) obj).x2 &&
                                        y1 == ((Link) obj).y2 &&
                                        x2 == ((Link) obj).x1 &&
                                        y2 == ((Link) obj).y1)

                );
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(new int[]{(x1 * x2 * 321), (y1 * y2 * 256)});
    }
}
