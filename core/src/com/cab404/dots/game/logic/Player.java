package com.cab404.dots.game.logic;

import com.badlogic.gdx.graphics.Color;

/**
 * @author cab404
 */
public class Player {
    public int can_place = 42;
    public boolean isAI = false;

    public final Color marker;

    public Player(Color marker) {
        this.marker = marker;
    }

}
