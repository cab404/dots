package com.cab404.dots.game.logic;

import com.cab404.dots.game.processor.DotsProcessor;
import com.cab404.dots.game.processor.Marker;

import java.util.*;


/**
 * @author cab404
 */
public class AI {

    private static class Move implements Comparable<Move> {
        float priority;
        Marker move;

        @SuppressWarnings("NullableProblems")
        @Override
        public int compareTo(Move move) {
            return (int) Math.signum(move.priority - priority);
        }

        private Move(float priority, Marker move) {
            this.priority = priority;
            this.move = move;
        }
    }

    public static Marker move(Player player, DotsProcessor core) {
        ArrayList<Move> moves = new ArrayList<>();

        List<Set<Marker>> sectors = core.getSectors(player.marker);

        HashMap<Marker, Float> sector_modifier = new HashMap<>();


        for (Set<Marker> sector : sectors) {
            Set<Marker> bounds = core.getBounds(sector);

            float sector_completion_size = 0;

            for (Marker boundary_entry : bounds)
                if (player.marker.equals(boundary_entry.owner))
                    sector_completion_size++;

            sector_completion_size = bounds.size() - sector_completion_size + ((float) sector.size() / 1000);

            for (Marker boundary_entry : bounds)
                if (boundary_entry.owner == null) {
                    sector_modifier.put(boundary_entry, sector_completion_size);

                    for (Marker anti_aliasing : core.getFullyCompoundCells(boundary_entry.x, boundary_entry.y)) {
                        if (anti_aliasing.owner == null && core.friends(anti_aliasing, player.marker) == 2) {
                            sector_modifier.put(anti_aliasing, sector_completion_size);
                        }
                    }

                }
        }


        for (int x = 0; x < core.w; x++) {
            for (int y = 0; y < core.h; y++) {
                Marker processing = core.get(x, y);

                float bonus = 0;

                if (processing.owner != null) continue;

                List<Marker> compound = core.getCompoundCells(x, y);

                float checked = core.findMoveWeight(player.marker, processing);
//                if (checked == 0)
//                    for (Object enemy : core.enemies(processing, player.marker)) {
//                        processing.owner = enemy;
//                        for (Marker marker : core.getCompoundCells(processing.x, processing.y)) {
//                            checked = Math.max((float) core.findMoveWeight(enemy, marker), checked * 2) / 2;
//                        }
//                    }

                float enemy_bonus = 0;
                for (Marker marker : compound) {
                    if (marker.owner != null)
                        if (!marker.owner.equals(player.marker))
                            enemy_bonus++;
                }
                if (enemy_bonus > 0 && enemy_bonus < 3)
                    enemy_bonus *= 3;
                else
                    enemy_bonus = 0;

                processing.owner = null;

//                if (mine == enemy && enemy == checked && checked == 0)
//                    moves.add(new Move(0, processing));
//                else

                float csm = (sector_modifier.containsKey(processing) ? sector_modifier.get(processing) : 0);

                moves.add(
                        new Move(
                                0
                                        + enemy_bonus
                                        + bonus
                                        + checked * 20
                                        + csm * 5
                                , processing
                        )
                );

            }
        }

        Collections.sort(moves);

        ArrayList<Move> max = new ArrayList<>();
        Move the_chosen_one = moves.get(0);

        for (Move move : moves) {
            if (move.priority == the_chosen_one.priority) {
                max.add(move);

            }
        }


        the_chosen_one = max.get((int) (Math.random() * max.size()));

        if (the_chosen_one.priority < 4)
            return core.get((int) (core.w * Math.random()), (int) (core.h * Math.random()));
        return the_chosen_one.move;

    }

}
