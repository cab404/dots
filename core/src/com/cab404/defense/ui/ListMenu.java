package com.cab404.defense.ui;

/** Красивые анимированные меню, yay.*/

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;
import com.cab404.dots.utils.U;
import com.cab404.guidy.Luna;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ListMenu extends GameObj implements Field {

    private class InternalField {
        public Field field;
        public float phase;
        public boolean forcedDeath = false;
    }

    private Vector2 real_size = new Vector2();
    public List<InternalField> fields;

    public float distance_between_fields = 1;
    public float animationSpeed = 0.25f;


    public ListMenu() {
        super();
        fields = new ArrayList<>();
    }

    public void add(float phase, Field... fields) {
        for (Field field : fields)
            add(field, phase);
    }

    public void add(Field... fields) {
        for (Field field : fields)
            add(field);
    }

    public void add(Field field) {
        add(field, 0);
    }

    public void add(Field field, float phase) {
        InternalField inf = new InternalField();
        inf.field = field;
        inf.phase = phase;
        fields.add(0, inf);
    }

    public void kill(int index) {
        fields.get(index).forcedDeath = true;
    }

    public void killall() {
        for (InternalField field : fields)
            field.forcedDeath = true;
    }

    public void reverse() {
        Collections.reverse(fields);
    }

    @Override
    public void render(SpriteBatch batch) {
        Vector2 drawingPoint = pos.cpy();
        Vector2 menu_size = new Vector2();

        for (InternalField entry : fields) {
            Vector2 size = entry.field.getSize(entry.phase);
            entry.field.render(batch, entry.phase, drawingPoint);
            drawingPoint.y += size.y + distance_between_fields;
            menu_size.y += size.y + distance_between_fields;

            menu_size.x = Math.max(size.x, menu_size.x);
        }
        real_size.set(menu_size);

    }


    @Override
    public void render(SpriteBatch batch, float phase, Vector2 lbc) {
        pos.set(lbc);
        render(batch);
    }

    @Override
    public Vector2 getSize(float phase) {
        return real_size.cpy();
    }

    @Override
    public boolean isDead() {
        return !isAlive;
    }

    @Override
    public void onMouseOver(Vector2 mouse) {
        Vector2 drawingPoint = new Vector2();

        for (InternalField entry : fields) {
            if (entry.field.isDead()) continue;
            Vector2 size = entry.field.getSize(entry.phase);

            Rectangle rect = new Rectangle();
            rect.set(drawingPoint.x, drawingPoint.y, size.x, size.y);

            if (rect.contains(mouse.x, mouse.y)) {
                entry.field.onMouseOver(mouse.cpy().sub(drawingPoint));
                break;
            }

            drawingPoint.y += size.y + distance_between_fields;
        }
    }


    /**
     * Запускает update во всех полях
     */
    public void updateFields(float time) {
        List<InternalField> removed = new LinkedList<>();
        for (InternalField entry : fields) {
            if (entry.field.isDead() || entry.forcedDeath) {
                entry.phase -= time / animationSpeed;
                if (entry.phase < 0) {
                    entry.phase = 0;
                    removed.add(entry);
                }
            } else {
                if (entry.phase < 1) {
                    entry.phase += time / animationSpeed;
                    if (entry.phase > 1) entry.phase = 1;
                }
                entry.field.update(time);
            }
        }
        fields.removeAll(removed);
    }


    @Override
    public void update(float time) {
        // Зачем я вынес updateFields в отдельный метод? Без понятия.
        updateFields(time);
        size.set(real_size.x, real_size.y);
        pos.set(pos.x, pos.y);

    }

}
