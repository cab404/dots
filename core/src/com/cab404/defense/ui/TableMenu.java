package com.cab404.defense.ui;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.objects.GameObj;

import java.util.ArrayList;
import java.util.List;

/**
 * Табличка с выбранным количеством столбцов. Поля расставляются так:
 * 7 8
 * 4 5 6
 * 1 2 3
 */

public class TableMenu extends GameObj implements Field {

    private class InternalField {
        public Vector2 size;
        public Vector2 pos;
        public float phase;
        public Field field;
        private boolean forcedDeath = false;

        private InternalField() {
            size = new Vector2();
            pos = new Vector2();
        }
    }

    private Vector2 real_size = new Vector2();
    public List<InternalField> fields;
    public Sprite bg;

    private float[] widths, heights;

    public float delim_x = 1, delim_y = 1;
    float animationSpeed = 0.25f;
    int columns = 1;


    public TableMenu(int columns) {
        super();
        bg = new TextField() {
            @Override
            public String getText() {
                return null;
            }
        }.bg;
        Color bgc = Color.GRAY.cpy();
        bgc.a = 0.5f;
        bg.setColor(bgc);

        fields = new ArrayList<>();
        this.columns = columns;
    }

    public void add(float phase, Field... fields) {
        for (Field field : fields)
            add(field, phase);
    }

    public void add(Field... fields) {
        for (Field field : fields)
            add(field);
    }

    public void add(Field field) {
        add(field, 0);
    }

    public void add(Field field, float phase) {
        InternalField inf = new InternalField();
        inf.field = field;
        inf.phase = phase;
        fields.add(0, inf);
    }

    public void kill(int index) {
        fields.get(index).forcedDeath = true;
    }

    public void killall() {
        for (InternalField field : fields)
            field.forcedDeath = true;
    }

    @Override
    public void render(SpriteBatch batch) {
        render(batch, 1, pos);
    }


    @Override
    public void render(SpriteBatch batch, float phase, Vector2 lbc) {
        pos.set(lbc);
        Vector2 drawingPoint = pos.cpy();
        Vector2 menu_size = new Vector2();
        bg.setSize(real_size.x, real_size.y);
        bg.setPosition(pos.x, pos.y);
        bg.draw(batch);

        int w = columns;
        int h = (int) Math.ceil((float) fields.size() / (float) w);

        widths = new float[w];
        heights = new float[h];

        for (int x = 0; x < w; x++)
            for (int y = 0; y < h; y++) {
                int index = x + w * y;

                if (index >= fields.size()) continue;
                InternalField cell = fields.get(index);

                Vector2 size = cell.field.getSize(cell.phase * phase);
                cell.size = size;

                widths[x] = Math.max(widths[x], size.x);
                heights[y] = Math.max(heights[y], size.y);

            }

        for (float width : widths) menu_size.x += width + delim_x;
        for (float height : heights) menu_size.y += height + delim_y;


        for (int x = 0; x < w; x++) {
            for (int y = 0; y < h; y++) {

                int index = x + w * y;

                if (index >= fields.size()) continue;
                InternalField cell = fields.get(index);

                cell.pos.add(drawingPoint.cpy().sub(cell.pos));
                cell.field.render(batch, cell.phase, cell.pos);
                drawingPoint.y += heights[y] + delim_y;

            }

            drawingPoint.x += widths[x] + delim_x;
            drawingPoint.y = pos.y;

        }

        real_size.set(menu_size);
    }

    @Override
    public Vector2 getSize(float phase) {
        return real_size.cpy();
    }

    @Override
    public boolean isDead() {
        return !isAlive;
    }

    @Override
    public void onMouseOver(Vector2 mouse) {
        Vector2 drawingPoint = new Vector2();

        for (int x = 0; x < widths.length; x++) {
            for (int y = 0; y < heights.length; y++) {

                int index = x + widths.length * y;
                if (index >= fields.size()) continue;

                InternalField cell = fields.get(index);
                if (cell.field.isDead()) continue;

                Rectangle rect = new Rectangle();

                rect.set(drawingPoint.x, drawingPoint.y, cell.size.x, cell.size.y);

                if (rect.contains(mouse.x, mouse.y)) {
                    cell.field.onMouseOver(mouse.cpy().sub(drawingPoint));
                    break;
                }


                drawingPoint.y += heights[y] + delim_y;
            }
            drawingPoint.x += widths[x] + delim_x;
            drawingPoint.y = 0;
        }

    }


    /**
     * Запускает update во всех полях
     */
    public void updateFields(float time) {
        for (InternalField entry : fields) {
            if (entry.field.isDead() || entry.forcedDeath) {
                entry.phase -= time / animationSpeed;
                if (entry.phase < 0) {
                    entry.phase = 0;
                    fields.remove(entry);
                }
            } else {
                if (entry.phase < 1) {
                    entry.phase += time / animationSpeed;
                    if (entry.phase > 1) entry.phase = 1;
                }
                entry.field.update(time);
            }
        }
    }


    @Override
    public void update(float time) {
        // Зачем я вынес updateFields в отдельный метод? Без понятия.
        updateFields(time);
        size.set(real_size.x, real_size.y);
        pos.set(pos.x, pos.y);

    }

}
