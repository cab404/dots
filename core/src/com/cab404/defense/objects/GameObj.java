package com.cab404.defense.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.storage.AbstractObjectStorage;

public abstract class GameObj {

    /**
     * Время с последнего кадра.
     */
    public Vector2 pos, size;
    public boolean isAlive = true;
    public AbstractObjectStorage parent;

    /**
     * Прорисовка элемента.
     */
    public abstract void render(SpriteBatch batch);


    /**
     * Обновление элемента.
     */
    public abstract void update(float time);


    /**
     * Вызывается сразу после уничтожения объекта. isDead выставлять вручную не
     * нужно.
     */
    public void onDeath() {
    }

    public Rectangle getBounds() {
        Rectangle box = new Rectangle();

        box.setWidth(size.x);
        box.setHeight(size.y);
        box.setX(pos.x);
        box.setY(pos.y);

        return box;
    }

    public GameObj() {
        pos = new Vector2();
        size = new Vector2();
    }

//
//    public void genExplosion(int particles) {
//        for (int i = 0; i != particles; i++) {
//            Particle prt = new Particle();
//            prt.stats.color = Math.random() > 0.5 ? Color.ORANGE.cpy() : Color.YELLOW.cpy();
//            centerObjectOnSelf(prt);
//
//            prt.var.way = new Vector2((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);
//
//            prt.stats.speed = (float) (stats.speed / 3 * (Math.random() + 0.5f));
//            prt.live = 0.5f;
//            prt.var.way.nor().add(var.way.cpy().mul(2));
//
//            var.parent.add(prt);
//        }
//    }


    public boolean overlaps(GameObj second_element) {
        Vector2 dot = pos.cpy().add(size.cpy().scl(2));

        boolean toReturn;
        float x = second_element.pos.x;
        float y = second_element.pos.y;
        float w = second_element.size.x;
        float h = second_element.size.y;

        toReturn = dot.x > x && dot.y > y && dot.x < x + w && dot.y < y + h;

        return toReturn;
    }

    public void centerObjectOnSelf(GameObj obj) {
        obj.pos = pos.cpy();
        obj.pos.add(new Vector2(
                (size.x - obj.size.x) / 2,
                (size.y - obj.size.y) / 2));
    }


    public boolean isInRange(GameObj obj, float range) {
        return obj.pos.dst2(pos) <= Math.pow(range, 2);
    }


    /**
     * Вызывается в update(), когда курсор находится в пределах var.img
     */
    public void onMouseOver(Vector2 mousePos) {
    }


    public String toString() {
        return this.getClass().toString() + ":" + pos.x + ":" + pos.y;
    }
}
