package com.cab404.defense.scene;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.cab404.guidy.Luna;

/**
 * Scene management class.
 *
 * @author cab404
 */
public class SceneManager implements ApplicationListener {
    // Кому нужны две активные сцены?
    protected Scene activeScene;
    /**
     * Суровая пауза. Отключает полностью прорисовку и обновление, для экономии ресурсов.
     */
    private boolean isPaused;

    public SceneManager(Scene startScene) {
        activeScene = startScene;
    }

    public boolean isPaused() {
        return isPaused;
    }


    @Override
    public void create() {

        Gdx.graphics.setVSync(true);
        activeScene.create();

    }


    @Override
    public void resize(int w, int h) {
        activeScene.resize(new Vector2(w, h));
    }


    @Override
    public void render() {
        if (!isPaused) {
            activeScene.update();
            activeScene.render();
        }
    }


    @Override
    public void pause() {
        isPaused = true;
    }


    @Override
    public void resume() {
        isPaused = false;
    }


    @Override
    public void dispose() {
        activeScene.dispose();
        System.gc();
    }

    public void changeScene(Scene target) {
        dispose();

        activeScene = target;

        target.create();
        target.resize(Luna.screen());

    }

    public Scene getCurrentScene() {
        return activeScene;
    }

}
