package com.cab404.defense.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.storage.AbstractObjectStorage;
import com.cab404.defense.storage.TeamObjectStorage;

public class SimpleScene implements Scene {

    public AbstractObjectStorage objects;

    public void dispose() {
    }

    /**
     * Вызывается один раз при создании сцены.
     */
    public void create() {
        objects = new TeamObjectStorage();
    }


    /**
     * Вызывается каждый раз при изменении размера экрана.
     */
    public void resize(Vector2 size) {
        objects.gfx.update();
    }

    /**
     * Вызывается для обновления элементов.
     */
    public void update() {
        objects.update(1, Gdx.graphics.getDeltaTime());
    }

    /**
     * Вызывается для прорисовки элементов.
     */
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20      .GL_DEPTH_BUFFER_BIT);
        objects.render();
    }


}
