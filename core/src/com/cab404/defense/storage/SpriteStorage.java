package com.cab404.defense.storage;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;
import java.util.Map;

/**
 * Используется для удобства организации загрузки и доступа к изображениям.
 * Помните, что доступ к картинкам по объекту String - операция дорогая. По
 * идее, два ключа должны более-менее ускорить работу, нежели хранить всё в
 * одном ObjectMap-е, да и доступ куда удобнее, но старайтесь реже доставать
 * картинки отсюда.
 *
 * @author cab404
 */
public class SpriteStorage {

    public Map<String, TileStorage> storage;

    /**
     * Координаты тайла на текстуре и сам тайл.
     *
     * @author cab404
     */
    public static class Tile {
        public int x, y, w, h;
        public String name;
        public Sprite tile;

        public Tile(int x, int y, int w, int h, String name) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.name = name;
        }
    }

    /**
     * Текстура с массивом тайлов для неё.
     *
     * @author cab404
     */
    public static class TileStorage {
        Texture texture;
        String texture_path;
        Map<String, Tile> tiles;

        public TileStorage(String texture_path) {
            this.texture_path = texture_path;
            tiles = new HashMap<>();
        }

        public void add(int x, int y, int w, int h, String name) {
            tiles.put(name, new Tile(x, y, w, h, name));
        }

        public void load() {
            texture = new Texture(Gdx.files.internal(texture_path));

            for (String tile_name : tiles.keySet()) {

                Tile tile = tiles.get(tile_name);
                tile.tile = new Sprite(new TextureRegion(texture, tile.x, tile.y, tile.w, tile.h));

            }
        }
    }

    /**
     * Добавляет текстуру для загрузки и тайлы к ней.
     *
     * @param name Название текстуры в хранилище
     */
    public void add(String name, TileStorage tiles) {
        storage.put(name, tiles);
    }

    public Sprite get(String texture, String tile) {
        return storage.get(texture).tiles.get(tile).tile;
    }

    public SpriteStorage() {
        storage = new HashMap<>();
    }

    public void load() {

        for (String key : storage.keySet()) {
            storage.get(key).load();
        }

    }

    public void dispose() {

        for (String key : storage.keySet()) {
            storage.get(key).texture.dispose();
        }

        storage.clear();
    }

}
