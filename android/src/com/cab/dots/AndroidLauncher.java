package com.cab.dots;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.cab404.dots.scene.FixedSceneManager;
import com.cab404.dots.scene.GameScene;
import com.cab404.dots.scene.MainMenu;
import com.cab404.guidy.Luna;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		Luna.scene = new FixedSceneManager(new MainMenu());
		initialize(Luna.scene, config);

//		initialize(new Dots(), config);
	}
}
